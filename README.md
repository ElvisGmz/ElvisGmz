### Hi there 👋, my name is Elvis
#### I am Web Developer JS FullStack
I love creating, destroying to understand and improve code

Skills: HTML / CSS / HTML / Vue.JS / React.JS / Node.JS / Git / GitHub / GitLab

- 🔭 I’m currently working on My Resume 
- 🌱 I’m currently learning Node.JS / ReactJS 


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/ElvisGmz)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg' alt='linkedin' height='40'>](https://www.linkedin.com/in/ElvisGmz/)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/codepen.svg' alt='codepen' height='40'>](https://codepen.io/ElvisGmz_)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/icloud.svg' alt='website' height='40'>](https://portafolio-gmz.web.app/)  

![GitHub stats](https://github-readme-stats.vercel.app/api?username=ElvisGmz&show_icons=true)  

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=ElvisGmz)](https://github.com/anuraghazra/github-readme-stats)

![Profile views](https://gpvc.arturio.dev/ElvisGmz)  
